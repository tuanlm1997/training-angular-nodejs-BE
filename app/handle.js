function returnError(res, data, e) {
  if (e) {
    return res.status(400).json({
      success: false,
      message: e.message || 'Have error', // Get message from new Error()
    });
  }

  return res.json({
    success: true,
    message: data?.message,
    data: data.data,
  });
}

module.exports = { returnError };
