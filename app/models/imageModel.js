const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const imageSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  img_name: String,
  img_url: String,
});
imageSchema.plugin(timestamps);

module.exports = mongoose.model('Images', imageSchema);
