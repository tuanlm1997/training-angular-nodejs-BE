const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const { Schema } = mongoose;

const productSchema = mongoose.Schema({
  _id: Schema.Types.ObjectId,
  product_code: String,
  name: String,
  type: String,
  category: String,
  description: String,
  price: Number,
  img_url: Array,
});
productSchema.plugin(timestamps);

module.exports = mongoose.model('Products', productSchema);
