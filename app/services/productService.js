const mongoose = require('mongoose');
const ProductModel = require('../models/productModel');

class ProductService {
  constructor() {
    this.ProductModel = ProductModel;
    this.typesData = [
      {
        type: 'Cars',
        cate: ['Car 1', 'Car 2', 'Car 3'],
      },
      {
        type: 'VietNam',
        cate: ['V-league', 'V-league1', 'V-league2'],
      },
      {
        type: 'England',
        cate: ['EPL', 'SkyBet'],
      },
      {
        type: 'France',
        cate: ['Ligue 1', 'Ligue 2'],
      },
      {
        type: 'Germany',
        cate: ['Bundesliga', 'Bundesliga 2'],
      },
      {
        type: 'Italia',
        cate: ['Seria A', 'Seria B'],
      },
      {
        type: 'Spain',
        cate: ['Laliga', 'Laliga B'],
      },
      {
        type: 'Others',
        cate: ['Portugal', 'Netherland', 'Russia'],
      },
    ];
  }

  async countImgById(id) {
    const sumImg = await this.ProductModel.findById(id);
    return {
      success: true,
      message: 'Count success',
      data: sumImg.img_url.length,
    };
  }

  async addProduct(body, url) {
    const firstProduct = await this.ProductModel.find({}).sort({ createdAt: -1 }).findOne();
    if (!firstProduct) {
      return [new Error('Data dont have any product!'), null];
    }
    const productCode = (`0000${parseInt(firstProduct.product_code.slice(-5), 10) + 1}`).slice(-5);
    try {
      const product = await new this.ProductModel({
        _id: new mongoose.Types.ObjectId(),
        product_code: productCode,
        name: body.name,
        type: body.type,
        description: body.description,
        price: body.price,
        category: body.category,
        img_url: { url, index: 0 },
      });
      product.save();
      return [null, {
        message: 'Insert product success',
        data: product,
      }];
    } catch {
      return [new Error('Cannot add product!'), null];
    }
  }

  async deleteImgProduct(id, index) {
    const response = await this.ProductModel.updateOne({ _id: id }, {
      $pull: {
        img_url: {
          index: {
            $in: [index[0], index[1], index[2], index[3]],
          },
        },
      },
    });
    return response.nModified;
  }

  async updateOneImage(id, url, index) {
    const response = await this.ProductModel.updateOne({ _id: id, 'img_url.index': index }, { $set: { 'img_url.$.url': url } });
    if (response.nModified === 0) {
      const push = await this.ProductModel
        .updateOne({ _id: id }, { $push: { img_url: { url, index } } });
      if (push.nModified === 0) {
        return {
          success: false,
          message: 'Cannot update image!',
        };
      }
      return {
        success: true,
        message: 'push success',
        data: push,
      };
    }
    return {
      success: true,
      message: 'modify success',
      data: response,
    };
  }

  async updateProduct(body) {
    try {
      const product = await this.ProductModel.findByIdAndUpdate(body.id, {
        name: body.name,
        type: body.type,
        description: body.description,
        price: body.price,
        category: body.category,
      }, { useFindAndModify: false, new: true });
      return [null, {
        message: 'Update product success',
        data: product,
      }];
    } catch {
      return [new Error('Cant update product'), null];
    }
  }

  async deleteProductById(id) {
    try {
      const result = await this.ProductModel.findByIdAndRemove(id, { useFindAndModify: false });
      return [null, { message: `Product '${result.name}' has been delete success`, data: result }];
    } catch {
      return [new Error('Product has been deleted'), null];
    }
  }

  async getById(id) {
    const result = await this.ProductModel.findById(id);
    return [null, {
      message: 'get by id success',
      data: result,
    }];
  }

  async search(typeData, categoryData, index, size, keyword) {
    let result = this.ProductModel.find({}).sort('-updatedAt');
    if (typeData) {
      result.where('type').equals(typeData);
    }
    if (categoryData) {
      result.where('category').equals(categoryData);
    }
    if (keyword) {
      result.find({
        $or: [
          { name: { $regex: keyword, $options: 'i' } }, { product_code: { $regex: keyword, $options: 'i' } },
        ],
      });
    }

    const count = await result.countDocuments();
    const dataResult = await this.ProductModel.find(result).skip(index * size).limit(size);
    result = await this.ProductModel.find({});
    return {
      success: true,
      message: 'search success',
      total: count,
      data: dataResult,
    };
  }

  async getType() {
    const type = [];
    this.typesData.forEach((el) => {
      type.push({ id: el.type, value: el.type });
    });
    return {
      success: true,
      message: 'Get type success',
      data: type,
    };
  }

  async getCate(req) {
    const cate = this.typesData.find((el) => el.type === req);
    if (!cate) {
      return [new Error('Type does not exist.'), null];
    }
    return [null, { data: cate.cate, message: 'Get cate success' }];
  }
}

module.exports = new ProductService();
