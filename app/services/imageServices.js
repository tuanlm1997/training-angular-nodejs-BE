const mongoose = require('mongoose');
const path = require('path');
const Resize = require('../../Resize');
const imageModel = require('../models/imageModel');

class ImageService {
  constructor() {
    this.ImageModel = imageModel;
  }

  async uploadImg(req, file) {
    try {
      const imagePath = path.join(__dirname, '../../public/images');
      const fileUpload = new Resize(imagePath);
      const filename = await fileUpload.save(file.buffer);
      const img = await new this.ImageModel({
        _id: new mongoose.Types.ObjectId(),
        img_name: filename,
        img_url: `${req.protocol}://${req.get('host')}/images/${filename}`,
      });
      img.save();
      return [null, {
        message: 'Upload image success.',
        data: img,
      }];
    } catch (e) {
      return [new Error('Cannot add image!'), null];
    }
  }
}

module.exports = new ImageService();
