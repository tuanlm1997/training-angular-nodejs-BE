const mongoose = require('mongoose');
const UserModel = require('../models/userModel');

class UserService {
  constructor() {
    this.userModel = UserModel;
  }

  async add(body) {
    let user = await this.userModel.findOne({ username: body.username });
    if (user) {
      return {
        success: false,
        message: 'DUPLICATE USERNAME',
      };
    }
    user = await this.userModel({
      _id: new mongoose.Types.ObjectId(),
      username: body.username,
      password: body.password,
      active: body.active,
    });
    user.save();
    if (!user) {
      return {
        success: false,
        message: 'cannot insert',
      };
    }
    return {
      success: true,
      message: 'insert success',
      data: user,
    };
  }

  async getAll() {
    const users = await this.userModel.find({});
    if (!users) {
      return {
        success: false,
        message: 'user does not exist.',
      };
    }
    return {
      success: true,
      message: 'get all users success.',
      data: users,
    };
  }
}

module.exports = new UserService();
