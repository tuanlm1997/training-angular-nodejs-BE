const multer = require('multer');

const fileFilter = (req, file, cb) => {
  const match = ['image/png', 'image/jpeg', 'image/jpg'];
  if (match.indexOf(file.mimetype) === -1) {
    req.errFilterMsg = {
      msg: 'Only .png, .jpg and .jpeg format allowed!',
    };
    cb(null, false, req.errFilterMsg);
  }
  cb(null, true);
};
const upload = multer({
  fileFilter,
  limits: { fileSize: 4 * 1024 * 1024 },
});
function makeMulterUploadMiddleware(multerUploadFunction) {
  return (req, res, next) => {
    // eslint-disable-next-line consistent-return
    multerUploadFunction(req, res, (error) => {
      if (error?.code === 'LIMIT_UNEXPECTED_FILE') {
        return res.status(500).json({ ...error, message: 'JUST IMPORT ONE FILE' });
      }
      if (error) {
        return res.status(500).json(error);
      }
      if (req.errFilterMsg) {
        return res.status(500).json(req.errFilterMsg);
      }
      next();
    });
  };
}

const multerUploadMiddleware = makeMulterUploadMiddleware(upload.single('file'));
const multerUploadArrayMiddleware = makeMulterUploadMiddleware(upload.array('file'));

// const someRouter = Router();
// someRouter.post('', multerUploadMiddleware, (req, res) => {
//   // now body contains all the fields except the one with the file
//   res.send(req.body);
// });

module.exports = {
  upload,
  multerUploadMiddleware,
  multerUploadArrayMiddleware,
};
