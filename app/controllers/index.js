const express = require('express');
const UserController = require('./Http/UserController');

const router = express.Router();

router.use('', UserController);
// router.get('/upload', async (req, res) => {
//   await res.render('index');
// });

module.exports = router;
