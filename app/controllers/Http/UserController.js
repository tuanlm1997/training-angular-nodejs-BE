/* eslint-disable consistent-return */
/* eslint-disable no-await-in-loop */
const express = require('express');

const router = express.Router();
const { returnValid } = require('../../resValid');
const upload = require('../../Middlewares/uploadMiddleware');
const ProductService = require('../../services/productService');
const imageServices = require('../../services/imageServices');
const handle = require('../../handle');
const validator = require('../../../validator');

router.get('/get-type', async (req, res) => {
  const type = await ProductService.getType();
  res.json(type);
});

router.get('/get-cate/:type', async (req, res) => {
  const { type } = req.params;
  const [error, cate] = await ProductService.getCate(type);
  handle.returnError(res, cate, error);
});

router.post('/add-product', [
  upload.multerUploadMiddleware,
  validator.validate.validateProduct(),
], async (req, res) => {
  const valid = returnValid(req, res);
  if (!valid) {
    if (!req.file) {
      const err = new Error('Need import image');
      handle.returnError(res, null, err);
    } else {
      const [err, image] = await imageServices.uploadImg(req, req.file);
      if (!err) {
        const [error, product] = await ProductService.addProduct(req.body, image.data.img_url);
        handle.returnError(res, product, error);
      } else {
        handle.returnError(res, image, err);
      }
    }
  }
});

router.put('/update-product', [
  upload.multerUploadArrayMiddleware,
  validator.validate.validateProduct(),
  validator.validate.validateAdditionalProduct(),
], async (req, res) => {
  const valid = returnValid(req, res);
  if (!valid) {
    const { id } = req.body;
    const product = await ProductService.getById(id);
    if (!product[1]) {
      return handle.returnError(res, null, new Error('This id does not exist!'));
    }
    if (req.body.indexDelete) {
      const indexDelete = JSON.parse(req.body.indexDelete);
      // eslint-disable-next-line no-unused-vars
      const nModified = await ProductService.deleteImgProduct(id, indexDelete);
    }
    if (!req.files[0] || !req.body.index) {
      const [err, newProduct] = await ProductService.updateProduct(req.body);
      handle.returnError(res, newProduct, err);
    } else {
      const index = JSON.parse(req.body.index);
      const chkSuccess = [];
      for (let i = 0; i < req.files.length; i += 1) {
        const [err, imageObj] = await imageServices.uploadImg(req, req.files[i]);
        if (!err) {
          const addImgProduct = await ProductService
            .updateOneImage(id, imageObj.data.img_url, index[i]);
          if (addImgProduct.success) {
            chkSuccess.push(true);
          } else {
            chkSuccess.push(false);
          }
        }
      }
      if (chkSuccess.indexOf(false) === -1) {
        const [err, newProduct] = await ProductService.updateProduct(req.body);
        handle.returnError(res, newProduct, err);
      } else {
        res.status(400).json({ success: false, message: 'At least one image cannot insert!' });
      }
    }
  }
});

router.delete('/delete-product/:id', validator.validate.validateParamId(), async (req, res) => {
  const valid = returnValid(req, res);
  if (!valid) {
    const { id } = req.params;
    const [err, response] = await ProductService.deleteProductById(id);
    handle.returnError(res, response, err);
  }
});

router.get('/get-by-id/:id', validator.validate.validateParamId(), async (req, res) => {
  const valid = returnValid(req, res);
  if (!valid) {
    const { id } = req.params;
    const [err, product] = await ProductService.getById(id);
    handle.returnError(res, product, err);
  }
});

router.get('/search', validator.validate.validateQuerySearch(), async (req, res) => {
  const valid = returnValid(req, res);
  if (!valid) {
    const index = parseInt(req.query.index, 10);
    const size = parseInt(req.query.size, 10);
    const result = await ProductService.search(
      req.query.type, req.query.category, index, size, req.query.keyword,
    );
    res.json(result);
  }
});

module.exports = router;
