/* eslint-disable no-useless-escape */
const {
  body, param, query,
} = require('express-validator');

const validateProduct = () => [
  body('name')
    .notEmpty({ ignore_whitespace: true }).withMessage('name does not Empty')
    .isString()
    .withMessage('name must be string'),
  body('type')
    .notEmpty({ ignore_whitespace: true }).withMessage('type does not Empty')
    .isString()
    .withMessage('type must be string'),
  body('category')
    .notEmpty({ ignore_whitespace: true }).withMessage('category does not Empty')
    .isString()
    .withMessage('category must be string'),
  body('description')
    .notEmpty({ ignore_whitespace: true }).withMessage('description does not Empty')
    .isString()
    .withMessage('description must be string'),
  body('price').notEmpty().withMessage('price does not Empty')
    .isDecimal()
    .withMessage('price must be Decimal'),
];

const validateAdditionalProduct = () => [
  body('id').notEmpty({ ignore_whitespace: true }).withMessage('id does not Empty')
    .isMongoId()
    .withMessage('id must be MongoId'),
  body('index', 'index must be number array from 0 -> 4')
    .if(body('index').exists())
    .matches(/^(\[)[0-4]{0,}(\,[0-4]){0,}\]$/, 'g'),
  body('indexDelete', 'indexDelete must be number array from 1 -> 4')
    .if(body('indexDelete').exists())
    .matches(/^(\[)[1-4]{0,}(\,[1-4]){0,}\]$/, 'g'),
];

const validateParamId = () => [
  param('id', 'must be MongoId').isMongoId(),
];

const validateQuerySearch = () => [
  query('index').isInt().withMessage('Index must be number'),
  query('size').isInt().withMessage('Size must be number'),
  query('type')
    .if(query('type').exists())
    .isString()
    .withMessage('type must be string'),
  query('category')
    .if(query('category').exists())
    .isString()
    .withMessage('category must be string'),
  query('keyword')
    .if(query('keyword').exists())
    .isString()
    .withMessage('keyword must be string'),
];

const validate = {
  validateProduct,
  validateAdditionalProduct,
  validateParamId,
  validateQuerySearch,
};

module.exports = { validate };
