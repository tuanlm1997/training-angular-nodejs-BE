/* eslint-disable no-undef */
/* eslint-disable no-console */
// Define Dependencies
const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();

global.Env = process.env;
// Content
const app = express();
const cors = require('cors');

app.use(cors());

// connect database
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useCreateIndex: true,
  // useFindAndModify: false
}).then(() => {
  console.log('connection to database established');
}).catch((err) => {
  console.log(`db error ${err.message}`);
  process.exit(-1);
});

/** Set up bodyparser */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static('public'));
app.set('view engine', 'ejs');

// server
const http = require('http').Server(app);

// Config directory controller
const controllers = require('./app/controllers');

app.use(controllers);

http.listen(Env.PORT, () => {
  console.log(`Server run at port: ${Env.PORT}`);
});

// Cast 404 Errors and forward them to error handler
app.use((req, res, next) => {
  const err = new Error('Not found!');
  err.status = 404;
  next(err);
});

// Error handler function
app.use((req, res) => {
  const error = app.get('env') === 'development' ? err : {};
  const status = err.status || 500;

  // Respone to client
  res.status(status).json({
    error: {
      message: error.message,
    },
  });
});

// Modules exports
module.exports = app;
